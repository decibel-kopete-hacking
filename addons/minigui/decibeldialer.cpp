/*
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "decibeldialer.h"

#include <Decibel/ContactManager>
#include <Decibel/DBusNames>

#include <QtCore/QDebug>

#include <QtTapioca/StreamChannel>
#include <QtTapioca/Stream>

class DecibelDialerPrivate
{
public:
    DecibelDialerPrivate(QObject * parent) :
        contactMgr(new org::kde::Decibel::ContactManager(Decibel::daemon_service,
                                                             Decibel::daemon_contactmanager_path,
                                                             QDBusConnection::sessionBus(), parent)),
        last_state(QtTapioca::Stream::Paused)
    { }
    ~DecibelDialerPrivate() { }

    org::kde::Decibel::ContactManager * const contactMgr;

    QList<QtTapioca::Stream *> streams;

    QtTapioca::Stream::StreamState last_state;

    QString number;
private:
};

// ---------------------------------------------------------------

DecibelDialer::DecibelDialer(QObject * parent) :
    QObject(parent),
    d(new DecibelDialerPrivate(this))
{ }

DecibelDialer::~DecibelDialer()
{ delete d; }

void DecibelDialer::dial(const QString & number)
{
    QString uri = "snom://" + number;

    QDBusReply<Decibel::ChannelInfo> channel_info_reply =
            d->contactMgr->contactUrl(uri, int(QtTapioca::Channel::Stream), false);
    if (!channel_info_reply.isValid())
    {
        QDBusError error = channel_info_reply.error();
        qWarning() << "DBus Error while dialing:"
                   << error.type() << ":"
                   << error.message();
        return;
    }
    if (channel_info_reply.value().isNull())
    {
        qWarning() << "ChannelInfo isNull.";
        return;
    }
}

#include "decibeldialer.moc"
