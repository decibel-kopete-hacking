/*
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#if 0
#define TAPIOCA_HAS_DTMF
#endif

#include "decibelchannelhandler.h"

#include <Decibel/ContactManager>
#include <Decibel/DBusNames>

#include <QtCore/QDebug>

#include <QtTapioca/StreamChannel>
#include <QtTapioca/ChannelTarget>
#include <QtTapioca/UserContact>
#include <QtTapioca/ContactGroup>
#include <QtTapioca/Connection>
#include <QtTapioca/Handle>
#if defined TAPIOCA_HAS_DTMF
#    include <QtTelepathy/Client/ChannelDTMFInterface>
#endif

class DecibelChannelHandlerPrivate
{
public:
    DecibelChannelHandlerPrivate() :
        last_state(QtTapioca::Stream::Paused)
    { }
    ~DecibelChannelHandlerPrivate() { }

    QList<QtTapioca::StreamChannel *> streamChannels;
    QtTapioca::Stream::StreamState last_state;

    QString getNumberFromStream(QtTapioca::StreamChannel * const stream) const
    {
        qDebug() << "Getting number from StreamChannel.";
        if (0 == stream) { return QString(); }

        QString result("unknown");
        return result;
#if 0
        Q_ASSERT(0 != stream->connection());

        QtTapioca::UserContact * myself(stream->connection()->userContact());
        qDebug() << "Connection state is:" << stream->connection()->status();
        if (0 == myself) { return result; }
        if (0 != stream->contactGroup()) { return result; }
        QList<QtTapioca::Contact *> contacts(stream->contactGroup()->contacts());
        Q_ASSERT(contacts.size() == 1);
        Q_ASSERT(0 != contacts[0]);
        if (*(contacts[0]) == *myself)
        {
            qDebug() << "... outgoing call...";
            // I am in the contact list, so this is an outgoing call:
            contacts = stream->contactGroup()->pendingContacts();
            Q_ASSERT(!contacts.isEmpty());
            Q_ASSERT(0 != contacts[0]);
            Q_ASSERT(0 != contacts[0]->handle());
            return  contacts[0]->handle()->inspect();
        }
        else
        {
            qDebug() << "... incoming call...";
            // I must be in the pending list =>
            //     incoming call, get number from here.;
            Q_ASSERT(0 != contacts[0]->handle());
            return contacts[0]->handle()->inspect();
        }
        return result;
#endif
    }
private:
};

// ---------------------------------------------------------------

DecibelChannelHandler::DecibelChannelHandler(QObject * parent) :
    Decibel::ChannelHandler(parent),
    d(new DecibelChannelHandlerPrivate())
{ }

DecibelChannelHandler::~DecibelChannelHandler()
{ delete d; }

bool DecibelChannelHandler::handleChannel(QtTapioca::Connection * connection,
                                          QtTapioca::Channel * channel,
                                          const bool)
{
    Q_ASSERT(connection != 0);
    Q_ASSERT(channel != 0);

    QtTapioca::StreamChannel * stream_channel =
        dynamic_cast<QtTapioca::StreamChannel *>(channel);
    if (0 == stream_channel)
    { return false; }

    connect(stream_channel, SIGNAL(closed()), this, SLOT(doChannelClosed()));
    connect(stream_channel, SIGNAL(newStream(QtTapioca::Stream *)),
            this, SLOT(doNumberOfStreamsChanged()));
    connect(stream_channel, SIGNAL(streamRemoved(QtTapioca::Stream *)),
            this, SLOT(doNumberOfStreamsChanged()));

    d->streamChannels.append(stream_channel);

    foreach (QtTapioca::Stream * stream, stream_channel->streams())
    {
        connect(stream, SIGNAL(stateChanged(QtTapioca::Stream *, QtTapioca::Stream::StreamState)),
                this, SLOT(doNumberOfStreamsChanged()));
        connect(stream, SIGNAL(streamError(QtTapioca::Stream *, int, const QString &)),
                this, SLOT(doStreamError(QtTapioca::Stream *, int, const QString &)));
    }

    doNumberOfStreamsChanged();

    return true;
}

void DecibelChannelHandler::hangUp()
{
    foreach (QtTapioca::StreamChannel * stream_channel, d->streamChannels)
    { stream_channel->close(); }
}

void DecibelChannelHandler::doNumberOfStreamsChanged()
{
    QtTapioca::Stream::StreamState state = QtTapioca::Stream::Paused;
    QString number;

    foreach (QtTapioca::StreamChannel * stream_channel, d->streamChannels)
    {
        foreach (QtTapioca::Stream * stream, stream_channel->streams())
        {
            switch(stream->state())
            {
            case QtTapioca::Stream::Paused:
                break;
            case QtTapioca::Stream::Connecting:
                if (state == QtTapioca::Stream::Paused)
                {
                    state = QtTapioca::Stream::Connecting;
                    number = d->getNumberFromStream(stream_channel);
                }
                break;
            case QtTapioca::Stream::Playing:
                if (state == QtTapioca::Stream::Paused ||
                    state == QtTapioca::Stream::Connecting)
                {
                    state = QtTapioca::Stream::Playing;
                    number = d->getNumberFromStream(stream_channel);
                }
                break;
            default:
                // Should never get here!
                Q_ASSERT(false);
            }
        }
    }
    if (state != d->last_state)
    {
        d->last_state = state;
        switch(state)
        {
        case QtTapioca::Stream::Paused:
            emit disconnected();
            break;
        case QtTapioca::Stream::Connecting:
            emit ringing(number);
            break;
        case QtTapioca::Stream::Playing:
            emit connected(number);
            break;
        default:
            // Should never get here!
            Q_ASSERT(false);
        }
    }
}

void DecibelChannelHandler::doStreamError(QtTapioca::Stream * stream,
                                          int code, const QString & message)
{ qWarning() << "STREAM ERROR:" << stream << code << ":" << message; }

void DecibelChannelHandler::doChannelClosed()
{
    QtTapioca::StreamChannel * stream_channel = dynamic_cast<QtTapioca::StreamChannel *>(sender());
    if (0 == stream_channel) { return; }

    d->streamChannels.removeAll(stream_channel);
    doNumberOfStreamsChanged();
}

void DecibelChannelHandler::sendDTMF(const QChar event_char)
{
#if defined TAPIOCA_HAS_DTMF
    uchar event;
    if (event_char == '0')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_0; }
    else if (event_char == '1')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_1; }
    else if (event_char == '2')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_2; }
    else if (event_char == '3')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_3; }
    else if (event_char == '4')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_4; }
    else if (event_char == '5')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_5; }
    else if (event_char == '6')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_6; }
    else if (event_char == '7')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_7; }
    else if (event_char == '8')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_8; }
    else if (event_char == '9')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Digit_9; }
    else if (event_char == '*')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Asterisk; }
    else if (event_char == '#')
    { event = org::freedesktop::Telepathy::DTMF_EVENT_Hash; }
    else { return; }

    foreach (QtTapioca::StreamChannel * channel, d->streamChannels)
    { channel->sendDTMFTone(event); }
#else
    Q_UNUSED(event_char)
#endif
}

#include "decibelchannelhandler.moc"
