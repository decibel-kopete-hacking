/*+
 * A minigui for VoIP/CTI telephony
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/* See KCall in trunk/playground/pim for a better example of
 * how to work with calls.
 */

#if defined USE_KDE
#    include <KDE/KApplication>
#    include <KDE/KCmdLineArgs>
#    include <KDE/KAboutData>
#else
#    include <QtGui/QApplication>
#endif

#include <Decibel/AccountManager>
#include <Decibel/DBusNames>

#include "keypad.h"
#include "decibeldialer.h"
#include "decibelchannelhandler.h"


int main(int argc, char ** argv)
{
    // Make Decibel types known: Otherwise we get DBus errors about unexpected
    // reply types.
    Decibel::registerTypes();

#if defined USE_KDE
    KAboutData aboutData( "minigui", I18N_NOOP("miniGUI"),
        "0.0.1",
        I18N_NOOP("A minimalistic VoIP/CTI application"),
        KAboutData::License_GPL,
        I18N_NOOP("(c) 2007, basysKom GmbH"));

    aboutData.addAuthor("Tobias Hunger", I18N_NOOP("Current Maintainer"),
                        "info@basyskom.de");

    KCmdLineArgs::init(argc, argv, &aboutData);
    // KCmdLineArgs::addCmdLineOptions(options);

    KApplication app;
#else
    QApplication app(argc, argv);
#endif

    KeyPad ui;
    DecibelDialer dialer;
    DecibelChannelHandler channel_handler;

    QObject::connect(&ui, SIGNAL(dial(QString)),
                     &dialer, SLOT(dial(QString)));
    QObject::connect(&ui, SIGNAL(sendDTMF(QChar)),
                     &channel_handler, SLOT(sendDTMF(QChar)));
    QObject::connect(&channel_handler, SIGNAL(ringing(QString)),
                     &ui, SLOT(ringing(QString)));
    QObject::connect(&channel_handler, SIGNAL(connected(QString)),
                     &ui, SLOT(connected(QString)));
    QObject::connect(&channel_handler, SIGNAL(disconnected()),
                     &ui, SLOT(disconnected()));

    QObject::connect(&ui, SIGNAL(hangUp()), &channel_handler, SLOT(hangUp()));

    // register Channel Handler on D-BUS:
    QDBusConnection::sessionBus().registerService(Decibel::organization_name + '.' +
                                                  "minigui");
    QDBusConnection::sessionBus().registerObject("/StreamChannelHandler",
                                                 &channel_handler);

    ui.show();

    return app.exec();
}

