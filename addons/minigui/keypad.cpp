/*
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "keypad.h"

KeyPad::KeyPad(QWidget * parent) :
    QMainWindow(parent)
{
    setupUi(this);

    disconnected();

    connect(dialButton, SIGNAL(clicked()), this, SLOT(doDial()));
    connect(hangUpButton, SIGNAL(clicked()), this, SLOT(doHangUp()));
    connect(button0, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button1, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button2, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button3, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button4, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button5, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button6, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button7, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button8, SIGNAL(clicked()), this, SLOT(doKeyPress()));
    connect(button9, SIGNAL(clicked()), this, SLOT(doKeyPress()));

    connect(numberEdit, SIGNAL(returnPressed()),
            dialButton, SIGNAL(clicked()));
}

KeyPad::~KeyPad()
{ }

void KeyPad::doKeyPress()
{
    QObject * button = sender();
    if (0 == button) { return; }

    QString button_name = button->objectName();
    if (!button_name.startsWith("button") ||
        button_name.size() != 7) { return; }

    if (isConnected)
    { emit sendDTMF(button_name[6]); }
    else
    {
        QChar to_add = button_name[6];

        numberEdit->setText(numberEdit->text().append(to_add));
    }
}

void KeyPad::doDial()
{
    callInitialized = true;
    emit dial(numberEdit->text());
}

void KeyPad::doHangUp()
{ emit hangUp(); }

void KeyPad::connected(const QString & number)
{
    dialButton->setEnabled(false);
    hangUpButton->setEnabled(true);
    numberEdit->setText(number);
    isConnected = true;
    statusbar->showMessage(tr("Connected to %1",
                              "1: phone number").arg(number));
}

void KeyPad::ringing(const QString & number)
{
    dialButton->setEnabled(false);
    hangUpButton->setEnabled(true);
    numberEdit->setText(number);
    if (callInitialized)
    {
        statusbar->showMessage(tr("Ringing %1",
                                  "1: phone number").arg(number));
    }
    else
    {
        statusbar->showMessage(tr("Incoming call from %1",
                                  "1: phone number").arg(number));
    }
}

void KeyPad::disconnected()
{
    isConnected = false;
    dialButton->setEnabled(true);
    hangUpButton->setEnabled(false);
    statusbar->showMessage(tr("Not connected."));
    callInitialized = false;
}

#include "keypad.moc"
