/*
 * Copyright (C) 2007 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef KEYPAD_H
#define KEYPAD_H

#include "ui_keypad.h"
#include <QtGui/QMainWindow>

/**
A keypad widget.

 @author Tobias Hunger <info@basyskom.de>
*/
class KeyPad : public QMainWindow, private Ui::KeyPad
{
    Q_OBJECT

public:
    explicit KeyPad(QWidget * parent = 0);
    ~KeyPad();

Q_SIGNALS:
    void dial(const QString &);
    void hangUp();
    void sendDTMF(const QChar);

public Q_SLOTS:
    void ringing(const QString &);
    void connected(const QString &);
    void disconnected();

protected Q_SLOTS:
    void doKeyPress();
    void doDial();
    void doHangUp();

private:
    bool callInitialized;
    bool isConnected;
};

#endif
