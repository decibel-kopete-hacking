/*
 * Copyright (C) 2007 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef DECIBELDIALER_H
#define DECIBELDIALER_H

#include <QtCore/QObject>

namespace QtTapioca
{
class Stream;
}

class DecibelDialerPrivate;

/**
 * Hold a connection to the Decibel Daemon and trigger it
 * to dial for us.
 *
 * @author Tobias Hunger <info@basyskom.de>
 */
class DecibelDialer : public QObject
{
    Q_OBJECT

public:
    explicit DecibelDialer(QObject * parent = 0);
    ~DecibelDialer();

public Q_SLOTS:
    void dial(const QString &);

private:
    DecibelDialerPrivate * const d;
};

#endif
