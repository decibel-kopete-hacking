/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Axel Jaeger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "propertyview.h"
#include "propertydelegate.h"
#include "propertymodel.h"

PropertyView::PropertyView(QWidget* parent) : QTreeView(parent), m_model ( NULL )
{
    m_model = new PropertyModel(this);
    Q_CHECK_PTR (m_model);

    setModel(m_model);
    setItemDelegate(new PropertyDelegate(this));
}

PropertyView::~PropertyView()
{ }

PropertyModel* PropertyView::propertyModel()
{ return m_model; }

#include "propertyview.moc"
