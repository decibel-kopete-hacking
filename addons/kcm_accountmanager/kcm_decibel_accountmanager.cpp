/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "kcm_decibel_accountmanager.h"
#include "ui_configdialog.h"
#include "accountmodel.h"
#include "presencedelegate.h"

#include "accountsettingsdialog.h"

#include <klocale.h>
#include <kglobal.h>
#include <kparts/genericfactory.h>
#include <kconfig.h>
#include <kdebug.h>

#include <QtDBus/QtDBus>
#include <QtGui/QWidget>
#include <QtGui/QLayout>
#include <QtGui/QHeaderView>

namespace
{
    const int COL_HANDLE = 0;
    const int COL_PRESENCE = 3;
}

typedef KGenericFactory<KCMDecibelAccountManager, QWidget> KCMDecibelAccountManagerFactory;
K_EXPORT_COMPONENT_FACTORY(kcm_decibel_accountmanager,
                           KCMDecibelAccountManagerFactory("KCMDecibelAccountManager"))

class KCMDecibelAccountManagerPrivate
{
    public:
    KCMDecibelAccountManagerPrivate(QObject * parent) :
        dialog(new Ui::ConfigDialog),
        widget(new QWidget()),
        accountModel(new AccountModel(parent))
    { dialog->setupUi(widget); }

    ~KCMDecibelAccountManagerPrivate()
    {
        delete widget;
        delete dialog;
    }

    Ui::ConfigDialog * const dialog;
    QWidget * const widget;

    AccountModel * const accountModel;
};

KCMDecibelAccountManager::KCMDecibelAccountManager(QWidget *parent,
                                                   const QStringList&) :
    KCModule( KCMDecibelAccountManagerFactory::componentData(), parent),
    d(new KCMDecibelAccountManagerPrivate(this))
{

    KAboutData * about = new KAboutData("kcm_decibel_accountmanager", 0,
                                        ki18n("Decibel Account Management"), 0,
                                        KLocalizedString(),
                                        KAboutData::License_LGPL,
                                        ki18n("(C) 2007 basysKom GmbH"));
    about->addAuthor(ki18n("Tobias Hunger"), ki18n("Author"), "info@basyskom.de");
    about->addAuthor(ki18n("Lars Lischke"), ki18n("Add/Modify dialog author"), "info@basyskom.de");

    setAboutData(about);
    about = 0;

    setQuickHelp(i18n("Manage communication accounts"));

    // setup widget:
    QVBoxLayout *layout = new QVBoxLayout( this );
    layout->setMargin(0);
    layout->setSpacing(KDialog::spacingHint());

    layout->addWidget(d->widget);

    // setup view:
    d->dialog->accountView->setModel(d->accountModel);

    d->dialog->accountView->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter);
    d->dialog->accountView->horizontalHeader()->setSortIndicatorShown(true);
    d->dialog->accountView->horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);

    d->dialog->accountView->verticalHeader()->hide();

    d->dialog->accountView->selectRow(0);

    d->dialog->accountView->hideColumn(COL_HANDLE);

    d->dialog->accountView->setItemDelegateForColumn(COL_PRESENCE,
            new PresenceDelegate(this));

    // connect stuff:
    connect(d->dialog->addButton, SIGNAL(clicked()),
            this, SLOT(doAddAccount()));
    connect(d->dialog->deleteButton, SIGNAL(clicked()),
            this, SLOT(doDeleteAccount()));
    connect(d->dialog->modifyButton, SIGNAL(clicked()),
            this, SLOT(doModifyAccount()));
}

KCMDecibelAccountManager::~KCMDecibelAccountManager()
{ delete d; }

void KCMDecibelAccountManager::save()
{ }

void KCMDecibelAccountManager::load()
{ }

void KCMDecibelAccountManager::defaults()
{ }

void KCMDecibelAccountManager::doAddAccount()
{
    AccountSettingsDialog window(this);
    window.setWindowTitle(ki18n("Add Account").toString());
    window.exec();
}

void KCMDecibelAccountManager::doDeleteAccount()
{
    QModelIndexList indexes =
            d->dialog->accountView->selectionModel()->selectedIndexes();
    if (indexes.size() < 1) { return; }

    int row = indexes[0].row();
    d->accountModel->deleteAccount(row);

    d->dialog->accountView->selectRow(0);
}

void KCMDecibelAccountManager::doModifyAccount()
{
    kWarning()<<"doModify"<<endl;
    QModelIndexList indexes =
        d->dialog->accountView->selectionModel()->selectedIndexes();
    if (indexes.size() < 1) { return; }
    int row = indexes[0].row();
    int id =d->accountModel->data(d->accountModel->index(row,COL_HANDLE)).value<int>();

    AccountSettingsDialog window(this,id);
    window.setWindowTitle(ki18n("Modify Account").toString());
    window.exec();

}

#include "kcm_decibel_accountmanager.moc"
