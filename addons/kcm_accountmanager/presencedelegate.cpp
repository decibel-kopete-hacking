/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Axel Jaeger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "presencedelegate.h"
#include "accountmodel.h"

#include <QtCore/QMetaEnum>
#include <KDE/KComboBox>

#include <QtTapioca/ContactBase>

#include <klocale.h>
#include <kdebug.h>

class PresenceDelegatePrivate
{
public:
    PresenceDelegatePrivate()
    { }

    ~PresenceDelegatePrivate()
    { }

    QMetaEnum presenceEnum;

    QString getStringForIndex(const int index) const
    {
        QString result;
        QByteArray enum_name = presenceEnum.key(index);
        if (!enum_name.isEmpty())
        {  result = QString::fromUtf8(enum_name.data()); }
        return result;
    }

    int getValueForIndex(const int index) const
    { return presenceEnum.value(index); }

    QHash<int, int> valueToIndexMapping;

private:
    PresenceDelegatePrivate(const PresenceDelegatePrivate &); // no impl.
};

PresenceDelegate::PresenceDelegate(QObject * parent) :
    QItemDelegate(parent),
    d(new PresenceDelegatePrivate())
{
    d->presenceEnum = QtTapioca::ContactBase::staticMetaObject.
        enumerator(QtTapioca::ContactBase::staticMetaObject.
                   indexOfEnumerator("Presence"));
}

PresenceDelegate::~PresenceDelegate()
{ delete d; }

QWidget * PresenceDelegate::createEditor(QWidget *parent,
                                         const QStyleOptionViewItem & /* option */,
                                         const QModelIndex & /* index */) const
{
    KComboBox *editor = new KComboBox(parent);
    for (int i = 0; i < d->presenceEnum.keyCount(); ++i)
    {
        editor->addItem(d->getStringForIndex(i));
        d->valueToIndexMapping.insert(d->getValueForIndex(i), i);
    }

    return editor;
}

void PresenceDelegate::setEditorData(QWidget *editor,
                                     const QModelIndex &index) const
{
    KComboBox * comboBox = static_cast<KComboBox*>(editor);
    comboBox->setCurrentIndex(d->valueToIndexMapping[index.data(Qt::UserRole).toInt()]);
}

void PresenceDelegate::setModelData(QWidget * editor,
                                    QAbstractItemModel * model,
                                    const QModelIndex & index) const
{
    KComboBox * comboBox = static_cast<KComboBox*>(editor);
    model->setData(index, d->presenceEnum.value(comboBox->currentIndex()));
}

void PresenceDelegate::updateEditorGeometry(QWidget * editor,
                                            const QStyleOptionViewItem & option,
                                            const QModelIndex & /* index */) const
{ editor->setGeometry(option.rect); }

#include <presencedelegate.moc>
