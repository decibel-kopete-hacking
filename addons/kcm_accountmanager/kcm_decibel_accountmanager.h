/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef KCM_DECIBEL_ACCOUNTMANAGER_H
#define KCM_DECIBEL_ACCOUNTMANAGER_H

#include <kcmodule.h>

class KCMDecibelAccountManagerPrivate;

class KCMDecibelAccountManager : public KCModule
{
    Q_OBJECT

public:
    explicit KCMDecibelAccountManager(QWidget *parent=0,
                                      const QStringList& = QStringList());
    ~KCMDecibelAccountManager();
    virtual void save();
    virtual void load();
    virtual void defaults();

private Q_SLOTS:
    void doAddAccount();
    void doDeleteAccount();
    void doModifyAccount();

private:
    KCMDecibelAccountManagerPrivate * const d;
};

#endif // header guard

