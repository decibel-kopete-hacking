/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef KCM_DECIBEL_ACCOUNTMODEL_H
#define KCM_DECIBEL_ACCOUNTMODEL_H

#include <QtCore/QAbstractTableModel>

class AccountModelPrivate;

class AccountModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit AccountModel(QObject * parent = 0);
    virtual ~AccountModel();

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    int columnCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex & index, const QVariant & value,
                 int role = Qt::EditRole);
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex & index) const;

    uint getHandleOfRow(const int) const;
    QString getDisplayNameOfRow(const int) const;

    void addAccount(const QVariantMap & data);
    void deleteAccount(const int row);
    void updateAccount(const int row, const QVariantMap & data);

Q_SIGNALS:
    void decibelNotFound();

protected Q_SLOTS:
    void decibelLost();

    void doAccountCreated(const uint);
    void doAccountUpdated(const uint);
    void doAccountDeleted(const uint);

    void doDataReset();

private:
    AccountModel(const AccountModel &); // no impl.

    AccountModelPrivate * const d;
};

#endif // header guard

