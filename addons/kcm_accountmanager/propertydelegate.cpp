/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Axel Jaeger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "propertydelegate.h"
#include "propertymodel.h"

#include <QCheckBox>
#include <QSpinBox>
#include <kdebug.h>
PropertyDelegate::PropertyDelegate(QWidget* parent) : QItemDelegate (parent)
{ }

PropertyDelegate::~PropertyDelegate()
{ }

QWidget* PropertyDelegate::createEditor(QWidget * parent,
                                        const QStyleOptionViewItem & option,
                                        const QModelIndex & index ) const
{
    const PropertyModel* model = qobject_cast<const PropertyModel*>(index.model());
    if (model != 0)
    { return QItemDelegate::createEditor(parent, option, index); }
    return 0;
}

#include "propertydelegate.moc"
