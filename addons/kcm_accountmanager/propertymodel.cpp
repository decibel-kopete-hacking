/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Axel Jaeger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "propertymodel.h"
#include <QtTapioca/ConnectionManager>
#include <kdebug.h>


PropertyModel::PropertyModel(QObject* parent) :
    QAbstractListModel(parent),
    currentId(0)
{ }

PropertyModel::~PropertyModel()
{ }

void PropertyModel::addProperty(const QString & title, const QVariant & value,
                                int flags)
{
    data_struct data;
    data.id = ++currentId;
    data.title = title;
    data.value = value;
    data.flags = flags;


    beginInsertRows(QModelIndex(), m_data.count(), m_data.count());
    m_data.append(data);
    endInsertRows();
}

void PropertyModel::setPropertyTitle(const uint id, const QString & title)
{
    int row = findId(id);
    if (row >= 0)
    {
        QModelIndex targetIndex = index(row, CT_Label);
        setData(targetIndex, title, Qt::DisplayRole);
    }
}

void PropertyModel::setPropertyValue(const uint id, const QVariant & value)
{
    int row = findId(id);
    if (row >= 0)
    {
        QModelIndex targetIndex = index(row, CT_Value);
        setData(targetIndex, value, Qt::DisplayRole);
    }
}


QVariant PropertyModel::propertyValue(const uint id) const
{
    int row = findId(id);
    if (row >= 0) { return m_data[row].value; }
    return QVariant();
}

QVariant PropertyModel::data(const QModelIndex &index, int role) const
{

    if (index.isValid())
    {
        int row = index.row();
        int count = m_data.count();
        if (row < count)
        {
            switch(index.column())
            {
            case CT_Label:
                if (role == Qt::DisplayRole)
                { return m_data[row].title; }
                break;
            case CT_Value:
                if (m_data[row].value.type() == QVariant::Bool)
                {
                    if (Qt::DisplayRole == role)
                    { return QVariant(); }
                    if (Qt::CheckStateRole == role)
                    { return m_data[row].value.toBool() ? Qt::Checked : Qt::Unchecked; }
                    if (Qt::UserRole == role)
                    { return m_data[row].value.toBool(); }
                }
                if (Qt::DisplayRole == role || Qt::EditRole == role || Qt::UserRole == role)
                { return m_data[row].value; }

                break;
            case CT_Flag_Required:
                if (role == Qt::CheckStateRole)
                {
                    return ((m_data[row].flags &
                            QtTapioca::ConnectionManager::Parameter::Required)!=0);
                }
                break;
            case CT_Flag_Register:
                if (role == Qt::CheckStateRole)
                {
                    return ((m_data[row].flags &
                            QtTapioca::ConnectionManager::Parameter::Register)!=0);
                }
                break;
            case CT_Flag_HasDefault:
                if (role == Qt::CheckStateRole)
                {
                    return ((m_data[row].flags &
                            QtTapioca::ConnectionManager::Parameter::HasDefault)!=0);
                }
                break;
            default:
                break;
            }
        }
        else
        { kWarning(780) << "PropertyModel::data: index.row() >= ids.count()" << endl; }
    }
    else
    { kWarning(780) << "PropertyModel::data: Index is invalid" << endl; }

    return QVariant();
}

QString PropertyModel::propertyTitle(const uint id) const
{
    int row = findId(id);
    if (row >= 0) { return m_data[row].title; }
    return QString();
}

int PropertyModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_data.count();
}

int PropertyModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return CT_MAX;
}

Qt::ItemFlags PropertyModel::flags(const QModelIndex & index) const
{
    switch (index.column())
    {
    case CT_Label:
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        break;
    case CT_Value:
        if (m_data[index.row()].value.type()==QVariant::Bool)
        { return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled |Qt::ItemIsEditable; }
        else
        { return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled; }
        break;
    default:
        return 0;
    }
}

bool PropertyModel::setData(const QModelIndex & index, const QVariant & value,
                            int role)
{
    if (index.column() == CT_Value)
    {
        if (Qt::DisplayRole == role || Qt::EditRole == role || Qt::CheckStateRole == role)
        {
            if (m_data[index.row()].value.type()==QVariant::Bool)
            {
                if (value==Qt::Unchecked)
                { m_data[index.row()].value=false; }
                else
                { m_data[index.row()].value=true; }
            }
            else
            { m_data[index.row()].value = value; }

            emit dataChanged(index, index);
            return true;
        }
        else
        {
            kWarning(780) << "PropertyModel::setData: Cannot set data for role != (Qt::DisplayRole == role || Qt::EditRole == role)";
        }
    }
    else
    { qWarning("PropertyModel::setData: Cannot set data for column != CT_Value"); }
    return false;
}

QVariant PropertyModel::headerData(int section, Qt::Orientation orientation,
                                   int role) const
{
    if (orientation == Qt::Horizontal && Qt::DisplayRole == role)
    {
        switch (section)
        {
        case CT_Label:
            return QVariant(tr("Name"));
            break;
        case CT_Value:
            return QVariant(tr("Value"));
            break;
        case CT_Flag_Required:
            return QVariant(tr("Required"));
            break;
        case CT_Flag_Register:
            return QVariant(tr("Register"));
            break;
        case CT_Flag_HasDefault:
            return QVariant(tr("HasDefault"));
            break;
        default:
            return 0;
        }
    }
    return QVariant();
}

void PropertyModel::deleteProperty(const uint id)
{
    int row = findId(id);

    beginRemoveRows(QModelIndex(), row, row);
    m_data.removeAt(row);
    endRemoveRows();
}

void PropertyModel::dump()
{ }

int PropertyModel::findId(const uint id) const
{
    int i;
    for (i = 0; i < m_data.count(); ++i)
    {
        if (m_data[i].id == id) { return i; }
    }
    return -1;
}

#include "propertymodel.moc"
