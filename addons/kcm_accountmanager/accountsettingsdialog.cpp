/*
 * Copyright (C) 2007 by basyskom GmbH
 *  @author Lars Lischke <lischke@gmx.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "accountsettingsdialog.h"
#include "propertymodel.h"

#include <Decibel/ProtocolManager>
#include <Decibel/DBusNames>
#include <Decibel/accountmanager.h>
#include <kdebug.h>

#include <QtTapioca/connectionmanagerfactory.h>
#include <QtTapioca/ConnectionManager>
#include <QVariant>
#include <QAbstractItemModel>
#include <QAbstractListModel>

class AccountSettingsDialogPrivate
{
public:
    AccountSettingsDialogPrivate(QObject * parent, int r) :
        protocolMgr(new org::kde::Decibel::ProtocolManager(Decibel::daemon_service,
                    Decibel::daemon_protocolmanager_path,
                    QDBusConnection::sessionBus(), parent)),
        accountMgr(new org::kde::Decibel::AccountManager(Decibel::daemon_service,
                   Decibel::daemon_accountmanager_path,
                   QDBusConnection::sessionBus(), parent)),
        model(parent),
        row(r)
    { }

    ~AccountSettingsDialogPrivate()
    {
        delete protocolMgr;
        delete accountMgr;
    }

    org::kde::Decibel::ProtocolManager * const protocolMgr;
    org::kde::Decibel::AccountManager * const accountMgr;
    PropertyModel model;
    int row;
    QVariantMap initial_parameters;
private:
};

// ---------------------------------------------------------------

AccountSettingsDialog::AccountSettingsDialog(QWidget * parent, int row) :
    QDialog(parent),
    d(new AccountSettingsDialogPrivate(this, row))
{
    setupUi(this);
    connect(protocolCombo, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(chooseProtocol(QString)));
    treeView->setModel(&(d->model));

    QDBusReply<QStringList> reply = d->protocolMgr->supportedProtocols();
    if (!reply.isValid())
    {
        QDBusError error = reply.error();
        kWarning() << "DBus Error:"
                << error.type() << ":"
                << error.message()<<endl;
        return;
    }
    protocolCombo->addItems(reply.value());
    updateModel();
    d->initial_parameters=getCurrentParameters();
}


AccountSettingsDialog::~AccountSettingsDialog()
{ delete d; }

void AccountSettingsDialog::accept()
{
    if (allRequiredParametersAreSet() == true)
    {
        QVariantMap parameters_map=removeParmeters();

        if (0 == d->row)
        {
            QDBusReply<unsigned int> reply = d->accountMgr->addAccount(parameters_map);
            if (!reply.isValid())
            { kWarning() << "Could not add account." << endl; }
        }
        else
        {
            QDBusReply<void> reply =
                d->accountMgr->updateAccount(d->row, parameters_map);
            if (!reply.isValid())
            { kWarning() << "Could not edit account." << endl; }
        }
        done(0);
    }
    else
    { error_label->setText(ki18n("Required parameter(s) is/are empty!").toString()); }
}

QVariantMap AccountSettingsDialog::getCurrentParameters()
{
    QVariantMap parameters_map;
    for (int r = 0; r < (d->model.rowCount()); ++r)
    {
        QString model_parameter_name = d->model.data(d->model.index(r,
                PropertyModel::CT_Label),  Qt::DisplayRole).toString();
        QVariant model_parameter_value = d->model.data(d->model.index(r,
                PropertyModel::CT_Value), Qt::UserRole);

        parameters_map.insert(model_parameter_name, model_parameter_value);
    }
    parameters_map.insert("decibel_protocol", protocolCombo->currentText());
    parameters_map.insert("decibel_display_name", display_nameedit->text());
    return parameters_map;
}

void AccountSettingsDialog::updateModel()
{
    int number=d->row;
    QVariantMap user_parameters;
    QString protocol;
    if (d->row != 0) {
        user_parameters = d->accountMgr->queryAccount(number);

        protocol = user_parameters.value("decibel_protocol").toString();
        protocolCombo->addItem(protocol);
        protocolCombo->setEnabled(false);
        display_nameedit->setText(user_parameters.value("decibel_display_name").toString());
    }
    else
    {
        protocol=protocolCombo->currentText();
    }
    QList<QtTapioca::ConnectionManager::Parameter> def_parameters=getParameters(protocol);
    for (int i = 0; i < def_parameters.count(); ++i)
    {
        if (user_parameters.contains(def_parameters[i].name()) &&
           def_parameters[i].value() != user_parameters.value(def_parameters[i].name()))
         {
            d->model.addProperty(def_parameters[i].name(),
                                 user_parameters.value(def_parameters[i].name()),
                                 def_parameters[i].flags());
           }
           else
           {
               d->model.addProperty(def_parameters[i].name(),def_parameters[i].value(),
                                    def_parameters[i].flags());
           }
    }
}

bool AccountSettingsDialog::allRequiredParametersAreSet()
{
    QList<QtTapioca::ConnectionManager::Parameter> parameters =
            getParameters(protocolCombo->currentText());
    for (int r=0; r< (d->model.rowCount()); ++r)
    {
        if ((parameters[r].flags() &
             QtTapioca::ConnectionManager::Parameter::Required != 0 ) &&
            d->model.data(d->model.index(r, PropertyModel::CT_Value),
                                         Qt::UserRole).toString().isEmpty() )
        { return false; }
    }
    return true;
}

QVariantMap AccountSettingsDialog::removeParmeters()
{
    QVariantMap start_parameters=d->initial_parameters;
    QVariantMap parameters_map=getCurrentParameters();

    for (int r = 0; r<parameters_map.size();++r)
    {
        QString model_parameter_name = d->model.data(d->model.index(r,
                PropertyModel::CT_Label),  Qt::DisplayRole).toString();
        if (parameters_map.value(model_parameter_name) ==
            start_parameters.value(model_parameter_name))
        { parameters_map.remove(model_parameter_name); }
    }
    return parameters_map;
}



QList<QtTapioca::ConnectionManager::Parameter>
AccountSettingsDialog::getParameters(const QString & protocol_name) const
{
    QDBusReply<QString> cm_name = d->protocolMgr->defaultConnectionManagerForProtocol(protocol_name);

    QList<QtTapioca::ConnectionManager::Parameter> parameters;
    QtTapioca::ConnectionManager* connection_mgr =
        QtTapioca::ConnectionManagerFactory::self()->getConnectionManagerByName(cm_name.value());
    if (!connection_mgr)
        return parameters;

    parameters=connection_mgr->protocolParameters(protocol_name);

    return parameters;
}

#include "accountsettingsdialog.moc"
