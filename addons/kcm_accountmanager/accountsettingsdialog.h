 /*
 * Copyright (C) 2007 by basyskom GmbH
 *  @author Lars Lischke <lischke@gmx.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef ACCOUNTSETTINGSDIALOG_H
#define ACCOUNTSETTINGSDIALOG_H

#include "ui_accountsettingsdialog.h"
#include <QtGui/QDialog>
#include <QtTapioca/ConnectionManager>

class AccountSettingsDialogPrivate;
/**
 * @brief In the AccountSettingsDialog the user can create or modify an account.
 * In the AccountSettingsDialog the user can create or modify an account.
 * @author Lars Lischke <lischke@gmx.net>
 */
class AccountSettingsDialog : public QDialog, private Ui::AccountSettingsDialog
{
    Q_OBJECT

    public:
        /**
        * @brief Constructor.
        */
        explicit AccountSettingsDialog(QWidget * parent = 0, int row = 0);
       /**
        * @brief Destructor.
        */
        ~AccountSettingsDialog();

    public Q_SLOTS:
        /**
         * @brief Start writing parameters.
         * Execute updateAccount() or addAccount and colse the dialog if, isRequired() is true.
         */
        void accept();

    protected Q_SLOTS:
        /**
         * @brief Change account settings.
         * This method changes the settings of an chosen account.
         */
        QVariantMap getCurrentParameters();
        /**
         * @brief Set the parameters in the model.
         * Set all default or edited parameters in the model.
         */
        void updateModel();
        /**
         * @brief Check whether all required parameters are set.
         * @returns true if all required parameter values are given.
         * This method provides wrong entries in the AccountManager.
         */
        bool allRequiredParametersAreSet();
        /**
         * @brief Remove not changed parameters from a parameters map.
         * @returns a Map of Parameters without not changed parameters.
         * Remove all parameters which are not changed, to set only new values in the account.
         */
        QVariantMap removeParmeters();
        /**
         * @brief Get a list with all possible parameters for the chosen protocolmanager.
         * @param protocol The name of the chosen protocol.
         * @returns The list with all possible parameters.
         * Get a list (key, value) with all possible parameters for the chosen protocolmanager by the Connectionmanager.
         */
        QList<QtTapioca::ConnectionManager::Parameter> getParameters(const QString & protocol) const;
    private:
        AccountSettingsDialogPrivate * const d;
};

#endif
