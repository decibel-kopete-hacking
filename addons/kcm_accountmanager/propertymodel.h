/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Axel Jaeger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PROPERTYMODEL_H
#define PROPERTYMODEL_H

#include <QAbstractListModel>
#include <QStyleOptionViewItem>

class PropertyModel : public QAbstractListModel {
Q_OBJECT
public:

    enum ColumnType {
        CT_Label = 0,
        CT_Value,
        CT_Flag_Required,
        CT_Flag_Register,
        CT_Flag_HasDefault,
        CT_MAX
    };

    PropertyModel(QObject* parent);
    ~PropertyModel();

    void addProperty(const QString & title,  const QVariant & value, int flags);
    void deleteProperty(const uint id);

    void setPropertyTitle(const uint id, const QString & title);
    QString propertyTitle(const uint id) const;

    void setPropertyValue(const uint id, const QVariant & value);
    QVariant propertyValue(const uint id) const;

    void dump();

    //// Above: My own API
    //// Below: Reimplementing Qt-API
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData (const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags ( const QModelIndex & index ) const;

    QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
protected:


private:
    uint currentId;

    int findId(const uint id) const;

    struct data_struct
    {
        uint id;
        QString title;
        QVariant value;
        int flags;
    };
    QList<data_struct> m_data;
};


#endif

