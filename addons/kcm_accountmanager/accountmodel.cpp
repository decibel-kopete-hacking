/*
 * A KControl Module to manage accounts in Decibel
 * Copyright (C) 2006 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "accountmodel.h"

#include <Decibel/AccountManager>
#include <Decibel/DBusNames>
#include <Decibel/AccountData>

#include <QtDBus/QDBusConnection>
#include <QtCore/QDebug>
#include <QtCore/QMetaEnum>
#include <QtCore/QMetaObject>

#include <QtTapioca/ContactBase>

#include <klocale.h>
#include <kdebug.h>

namespace
{
    enum
    {
        COL_HANDLE = 0,
        COL_DISPLAY_NAME,
        COL_PROTOCOL,
        COL_PRESENCE,
        COL_CURRENT_PRESENCE,
        COL_AUTORECONNECT,
        COL_MAX
    };
}

class AccountModelPrivate
{
public:
    AccountModelPrivate()
    { }

    ~AccountModelPrivate()
    {
        delete accountMgr;
        accountMgr = 0;
    }

    org::kde::Decibel::AccountManager * accountMgr;

    QList<uint> accountIds;

    QVariant getPresenceVariant(const int presence) const
    {
        QVariant result;
        QByteArray enum_name = presenceEnum.valueToKey(presence);
        if (!enum_name.isEmpty())
        {  result = QVariant(QString::fromUtf8(enum_name.data())); }
        return result;
    }

    QMetaEnum presenceEnum;

private:
    AccountModelPrivate(const AccountModelPrivate &); // no impl.
};

AccountModel::AccountModel(QObject * parent) :
    QAbstractTableModel(parent),
    d(new AccountModelPrivate())
{
    d->presenceEnum = QtTapioca::ContactBase::staticMetaObject.
        enumerator(QtTapioca::ContactBase::staticMetaObject.
                   indexOfEnumerator("Presence"));
    if (!QDBusConnection::sessionBus().isConnected())
    {
        emit decibelNotFound();
        return;
    }

    Decibel::registerTypes();

    d->accountMgr = new org::kde::Decibel::AccountManager(
                            Decibel::daemon_service,
                            Decibel::daemon_accountmanager_path,
                            QDBusConnection::sessionBus(), this);

    if (0 == d->accountMgr) { emit decibelNotFound(); }

    QDBusReply<QList<uint> > account_list = d->accountMgr->listAccounts();
    if (!account_list.isValid()) { decibelLost(); }

    d->accountIds = account_list.value();

    // Connect:
    connect(d->accountMgr, SIGNAL(accountCreated(uint)),
            SLOT(doAccountCreated(uint)));
    connect(d->accountMgr, SIGNAL(accountUpdated(uint)),
            SLOT(doAccountUpdated(uint)));
    connect(d->accountMgr, SIGNAL(accountDeleted(uint)),
            SLOT(doAccountDeleted(uint)));
    connect(d->accountMgr, SIGNAL(dataReset()), SLOT(doDataReset()));
}

AccountModel::~AccountModel()
{ delete d; }

int AccountModel::rowCount(const QModelIndex & parent ) const
{
    if (parent.isValid()) { return 0; }
    if (0 == d->accountMgr) { return 0; }
    return d->accountIds.size();
}

int AccountModel::columnCount(const QModelIndex & parent ) const
{
    if (parent.isValid()) { return 0; }
    if (0 == d->accountMgr) { return 0; }

    return COL_MAX;
}

QVariant AccountModel::data(const QModelIndex & index, int role) const
{
    QVariant result;
    if (index.model() != this) { return result; }
    if (0 == d->accountMgr) { return result; }

    int row = index.row();
    if (row >= d->accountIds.size()) { return result; }
    int col = index.column();

    QDBusReply<QVariantMap> reply =
    d->accountMgr->queryAccount(d->accountIds[row]);
    QVariantMap account_data = reply.value();

    switch (role)
    {
    case Qt::DisplayRole:
        switch (col)
        {
        case COL_HANDLE:
            result = QVariant(d->accountIds[row]);
            break;
        case COL_DISPLAY_NAME:
            result = QVariant(account_data[Decibel::name_display_name].toString());
            break;
        case COL_PROTOCOL:
            result = QVariant(account_data[Decibel::name_protocol].toString());
            break;
        case COL_PRESENCE:
            result = d->getPresenceVariant(account_data[Decibel::name_presence].toInt());
            break;
        case COL_CURRENT_PRESENCE:
            result = d->getPresenceVariant(account_data[Decibel::name_current_presence].toInt());
            break;
        default:
            break;
        }
        break;
    case Qt::CheckStateRole:
        switch (col)
        {
        case COL_AUTORECONNECT:
            {
                int state = Qt::Unchecked;
                if (account_data[Decibel::name_autoreconnect].toBool())
                { state = Qt::Checked; }
                result = QVariant(state);
            }
            break;
        default:
            break;
        }
        break;
    case Qt::UserRole:
        switch (col)
        {
        case COL_PRESENCE:
            result = QVariant(account_data[Decibel::name_presence].toInt());
            break;
        case COL_CURRENT_PRESENCE:
            result = QVariant(account_data[Decibel::name_current_presence].toInt());
            break;
        default:
            result = data(index, Qt::DisplayRole);
            break;
        }
        break;
    default:
        break;
    }

    return result;
}

bool AccountModel::setData(const QModelIndex & index, const QVariant & value,
                           int role)
{
    bool result = false;
    if (index.model() != this) { return result; }
    int row = index.row();
    int col = index.column();

    kDebug() << "(" << row << col << "):" << value << "role=" << role;

    switch (col)
    {
    case COL_PRESENCE:
        if (Qt::EditRole == role)
        {
            QVariantMap data;
            data.insert(Decibel::name_presence, value.toInt());
            updateAccount(row, data);
            result = true;
        }
        break;
    case COL_AUTORECONNECT:
        if (Qt::CheckStateRole == role)
        {
            QVariantMap data;
            bool autoreconnect;
            if (value.toInt() == Qt::Unchecked) { autoreconnect = false; }
            else { autoreconnect = true; }
            data.insert(Decibel::name_autoreconnect, autoreconnect);
            updateAccount(row, data);
            result = true;
        }
        break;
    default:
        break;
    }
    return result;
}

QVariant AccountModel::headerData(int section,
                                  Qt::Orientation orientation,
                                  int role) const
{
    if (role != Qt::DisplayRole) { return QVariant(); }
    if (orientation != Qt::Horizontal) { return QVariant(); }

    Q_ASSERT(role == Qt::DisplayRole);
    Q_ASSERT(orientation == Qt::Horizontal);

    QVariant result;
    switch (section)
    {
    case COL_HANDLE:
        result = QVariant(i18n("Handle"));
        break;
    case COL_DISPLAY_NAME:
        result = QVariant(i18n("Account"));
        break;
    case COL_PROTOCOL:
        result = QVariant(i18n("Protocol"));
        break;
    case COL_PRESENCE:
        result = QVariant(i18n("Intended Presence"));
        break;
    case COL_CURRENT_PRESENCE:
        result = QVariant(i18n("Current Presence"));
        break;
    case COL_AUTORECONNECT:
        result = QVariant(i18n("Reconnect"));
        break;
    default:
        break;
    }

    return result;
}

Qt::ItemFlags AccountModel::flags(const QModelIndex & index) const
{
    Qt::ItemFlags item_flags = (Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    if (index.model() != this) { return item_flags; }
    int col = index.column();
    switch (col)
    {
    case COL_PRESENCE:
        item_flags |= Qt::ItemIsEditable;
        break;
    case COL_AUTORECONNECT:
        item_flags |= Qt::ItemIsUserCheckable;
        break;
    default:
        break;
    }
    return item_flags;
}

void AccountModel::decibelLost()
{
    delete(d->accountMgr);
    d->accountMgr = 0;
    emit decibelNotFound();
}

void AccountModel::doAccountCreated(const uint account_handle)
{
    Q_ASSERT(d->accountIds.indexOf(account_handle) < 0);
    beginInsertRows(QModelIndex(), d->accountIds.size(), d->accountIds.size());
    d->accountIds.append(account_handle);
    endInsertRows();
}

void AccountModel::doAccountUpdated(const uint account_handle)
{
    int position = d->accountIds.indexOf(account_handle);
    if (position >= 0)
    { emit dataChanged(index(position, 1), index(position, COL_MAX - 1)); }
}

void AccountModel::doAccountDeleted(const uint account_handle)
{
    int position = d->accountIds.indexOf(account_handle);
    if (position >= 0)
    {
        beginRemoveRows(QModelIndex(), position, position);
        int removed_num = d->accountIds.removeAll(account_handle);
        Q_ASSERT(removed_num == 1);
        endRemoveRows();
    }
}

uint AccountModel::getHandleOfRow(const int row) const
{
    if (row >= columnCount()) { return 0; }
    return d->accountIds[row];
}

QString AccountModel::getDisplayNameOfRow(const int row) const
{ return data(index(row, COL_DISPLAY_NAME)).toString(); }

void AccountModel::addAccount(const QVariantMap & /* data */ )
{
}

void AccountModel::deleteAccount(const int row)
{
    uint account_handle = getHandleOfRow(row);

    // FIXME: Add a confirmation dialog?
    d->accountMgr->deleteAccount(account_handle);
}

void AccountModel::updateAccount(const int row, const QVariantMap & data)
{
    uint account_handle = getHandleOfRow(row);

    // FIXME: Add a confirmation dialog?
    d->accountMgr->updateAccount(account_handle, data);
}

void AccountModel::doDataReset()
{
    d->accountIds.clear();
    reset();
}

#include <accountmodel.moc>
