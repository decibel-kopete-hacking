/*
 * the Decibel Realtime Communication Framework
 * Copyright (C) 2008 by basyskom GmbH
 *  @author Tobias Hunger <info@basyskom.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DECIBEL_KDE_EXPORT_H
#define DECIBEL_KDE_EXPORT_H

#include <QtCore/QtGlobal>

#ifdef decibel_KDE_EXPORTS 
# define DECIBEL_KDE_EXPORT Q_DECL_EXPORT
#else
# ifdef Q_OS_WIN
#  define DECIBEL_KDE_EXPORT Q_DECL_IMPORT
# else
#  define DECIBEL_KDE_EXPORT Q_DECL_EXPORT
# endif
#endif

#endif
