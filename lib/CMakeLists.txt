project(decibel-lib-kde)

find_package(KDE4 REQUIRED)
include(FindKdepimLibs)
include (KDE4Defaults)
find_package(Plasma REQUIRED)
add_definitions (${QT_DEFINITIONS} ${KDE4_DEFINITIONS})

SET(QT_DONT_USE_QTGUI "YES")

INCLUDE_DIRECTORIES(${QT_QTCORE_INCLUDE_DIR}
                    ${CMAKE_CURRENT_BINARY_DIR}
                    ${CMAKE_CURRENT_SOURCE_DIR}
                    ${KDE4_INCLUDE_DIR}
)

SET(decibel_kde_SRCS
    # Akonadi attributes.
    accessattribute.cpp
    #contactdataattribute.cpp
    # Decibel data objects
    #contactaccount.cpp
)

kde4_add_library(decibel_kde SHARED
                 ${decibel_kde_SRCS}
)
TARGET_LINK_LIBRARIES(decibel_kde
                      ${QT_LIBRARIES}
                      akonadi-kde
)

# Install:
INSTALL(TARGETS decibel_kde DESTINATION ${LIB_INSTALL_DIR})
