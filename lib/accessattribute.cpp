/*
 * the Decibel Realtime Communication Framework
 * Copyright (C) 2008 George Goldberg <grundleborg@googlemail.com>
 *  @author George Goldberg <grundleborg@googlemail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "accessattribute.h"

#include <akonadi/attribute.h>

#include <QtCore/QList>
#include <QtCore/QChar>
 
using namespace Decibel;
 
AccessAttribute::AccessAttribute(bool enabled, bool visible, bool remote)
  : m_enabled(enabled),
    m_visible(visible),
    m_remote(remote)
{ }

void
AccessAttribute::setEnabled(bool enabled)
{
    m_enabled = enabled;
}

bool
AccessAttribute::enabled() const
{
    return m_enabled;
}

void
AccessAttribute::setVisible(bool visible)
{
    m_visible = visible;
}

bool
AccessAttribute::visible() const
{
    return m_visible;
}

void
AccessAttribute::setRemote(bool remote)
{
    m_remote = remote;
}

bool
AccessAttribute::remote() const
{
    return m_remote;
}

QByteArray
AccessAttribute::type() const
{
    return "DecibelAccess";
}

Akonadi::Attribute*
AccessAttribute::clone() const
{
    return new AccessAttribute(m_enabled, m_visible, m_remote);
}

QByteArray
AccessAttribute::serialized() const
{
    QByteArray output;

    output.append("visible=");
    output.append(m_visible ? '1' : '0');
    output.append('\n');

    output.append("enabled=");
    output.append(m_enabled ? '1' : '0');
    output.append('\n');

    output.append("remote=");
    output.append(m_remote ? '1' : '0');
    output.append('\n');

    return output;
}

void
AccessAttribute::deserialize( const QByteArray &data )
{
    QList<QByteArray> lines = data.split('\n');

    foreach(QByteArray line, lines)
    {
        QByteArray value;
        if(line.startsWith("visible="))
        {
            value = line.right(line.size()-8);
            if(value == "1")
                m_visible = true;
            if(value == "0")
                m_visible = false;
        }
        else if(line.startsWith("enabled="))
        {
            value = line.right(line.size()-8);
            if(value == "1")
                m_enabled = true;
            if(value == "0")
                m_enabled = false;
        }
        else if(line.startsWith("remote="))
        {
            value = line.right(line.size()-7);
            if(value == "1")
                m_remote = true;
            if(value == "0")
                m_remote = false;
        }
    }
}

