/*
 * the Decibel Realtime Communication Framework
 * Copyright (C) 2008 George Goldberg <grundleborg@googlemail.com>
 *  @author George Goldberg <grundleborg@googlemail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DECIBEL_KDE_ACCESS_ATTRIBUTE_H
#define DECIBEL_KDE_ACCESS_ATTRIBUTE_H

#include "decibel_kde_export.h"

#include <akonadi/attribute.h>

#include <QtCore/QByteArray>

namespace Decibel {

    class DECIBEL_KDE_EXPORT AccessAttribute : public Akonadi::Attribute
    {
    public:
        /**
         * @param enabled Specifies whether access to this entity is enabled or
         *        disabled for decibel.
         * @param visible Specifies whether this entity is visible in
         *        decibel powered buddy lists.
         * @param remote Specifies whether this entity may be added to remote
         *        contact lists.
         */
        AccessAttribute(bool enabled = false,
                        bool visible = false,
                        bool remote = false);

        void setEnabled(bool enabled);
        bool enabled() const;
        void setVisible(bool visible);
        bool visible() const;
        void setRemote(bool remote);
        bool remote() const;

        virtual QByteArray type() const;
        virtual Akonadi::Attribute * clone() const;
        virtual QByteArray serialized() const;
        virtual void deserialize(const QByteArray & data);

    private:
        bool m_enabled;
        bool m_visible;
        bool m_remote;
    };

} //namespace

#endif  // Header guard

