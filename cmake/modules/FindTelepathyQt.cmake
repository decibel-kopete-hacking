# Try to find the Qt binding of the Telepathy library
# TELEPATHY_QT_FOUND - system has Telepathy-Qt
# TELEPATHY_QT_INCLUDE_DIR - the Telepathy-Qt include directory
# TELEPATHY_QT_LIBRARIES - Link these to use Telepathy-Qt

# Copyright (c) 2008, Allen Winter <winter@kde.org>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

SET (TELEPATHY_QT_FIND_REQUIRED ${TelepathyQt_FIND_REQUIRED})
if (TELEPATHY_QT_INCLUDE_DIR AND TELEPATHY_QT_LIBRARIES)
  # Already in cache, be silent
  set(TELEPATHY_QT_FIND_QUIETLY TRUE)
endif (TELEPATHY_QT_INCLUDE_DIR AND TELEPATHY_QT_LIBRARIES)

find_path(TELEPATHY_QT_INCLUDE_DIR
  NAMES QtTelepathy/Client/Channel QtTelepathy/Client/Connection)
find_library(TELEPATHY_QT_LIBRARIES NAMES QtTelepathyClient )

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(TELEPATHY_QT DEFAULT_MSG
                                  TELEPATHY_QT_LIBRARIES TELEPATHY_QT_INCLUDE_DIR)

mark_as_advanced(TELEPATHY_QT_INCLUDE_DIR TELEPATHY_QT_LIBRARIES)
