# Try to find the Qt binding of the Tapioca library
# TAPIOCA_QT_FOUND - system has Tapioca-Qt
# TAPIOCA_QT_INCLUDE_DIR - the Tapioca-Qt include directory
# TAPIOCA_QT_LIBRARIES - Link these to use Tapioca-Qt

# Copyright (c) 2008, Allen Winter <winter@kde.org>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

SET (TOPIOCA_QT_FIND_REQUIRED ${TopiocaQt_FIND_REQUIRED})
if (TAPIOCA_QT_INCLUDE_DIR AND TAPIOCA_QT_LIBRARIES)
  # Already in cache, be silent
  set(TAPIOCA_QT_FIND_QUIETLY TRUE)
endif (TAPIOCA_QT_INCLUDE_DIR AND TAPIOCA_QT_LIBRARIES)

find_path(TAPIOCA_QT_INCLUDE_DIR
  NAMES QtTapioca/channel.h QtTapioca/connection.h)
find_library(TAPIOCA_QT_LIBRARIES NAMES QtTapioca )

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(TAPIOCA_QT DEFAULT_MSG
                                  TAPIOCA_QT_LIBRARIES TAPIOCA_QT_INCLUDE_DIR)


mark_as_advanced(TAPIOCA_QT_INCLUDE_DIR TAPIOCA_QT_LIBRARIES)

