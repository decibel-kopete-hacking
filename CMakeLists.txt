# This project is decibel-kde - extras to Decibel that provide integration into
# the KDE4 environment.
project (decibel-kde)

# Extra FindFOO.cmake files are in this directory.
set (CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/modules)

# Depend on KDE4
find_package (KDE4 REQUIRED)
# Set KDE default variables
include (KDE4Defaults)
# Include useful KDE macros.
include (MacroLibrary)
include (MacroOptionalAddSubdirectory)
include (MacroOptionalDependPackage)

# telepathy-qt and tapioca-qt are required because we need them to build
# anything related to Decibel.
find_package (TelepathyQt REQUIRED)
find_package (TapiocaQt REQUIRED)

# Add KDE4 and Qt compiler definitions.
add_definitions (${QT_DEFINITIONS}
                 ${KDE4_DEFINITIONS}
)

# Set the standard include directories, including the KDE4 include directory
include_directories (${CMAKE_SOURCE_DIR}
                     ${CMAKE_BINARY_DIR}
                     ${QT_QTCORE_INCLUDE_DIR}
                     ${QT_QTGUI_INCLUDE_DIR}
                     ${QT_QTXML_INCLUDE_DIR}
                     ${TELEPATHY_QT_INCLUDE_DIR}
                     ${TAPIOCA_QT_INCLUDE_DIR}
                     ${KDE4_INCLUDES}
)

# COMPONENT_SEARCH_DIR is Decibel specific. Set it here for now, although it
# should probably be set in FindDecibel.cmake once that exists.
set (COMPONENT_SEARCH_DIR
     "${DATA_INSTALL_DIR}/Decibel/components"
     CACHE PATH "The subdirectory relative to the install prefix where Decibel will look for its components (default is ${DATA_INSTALL_DIR}/Decibel/components)."
     FORCE
)

# Add the subdirectories we will build.
add_subdirectory (lib)
add_subdirectory (plugins)
add_subdirectory (addons)
