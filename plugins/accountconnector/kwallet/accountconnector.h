/*
 * the Decibel Realtime Communication Framework
 * Copyright (C) 2008 by George Goldberg
 *  @author George Goldberg <grundleborg@googlemail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DECIBEL_DAEMON_KDE4_ACCOUNTCONNECTOR_H
#define DECIBEL_DAEMON_KDE4_ACCOUNTCONNECTOR_H

#include <Decibel/AccountConnectorBase>

namespace KWallet {
    class Wallet;
}
class KComponentData;

/// @cond FALSE

/**
 * @brief A KWallet based implementation of the account connector interface.
 *
 * @author George Goldberg <grundleborg@googlemail.com>
 */
class AccountConnector : public AccountConnectorBase
{
    Q_OBJECT
    Q_INTERFACES(AccountConnectorBase)

public:
    AccountConnector(QObject *parent = 0);
    ~AccountConnector();

    void openStorage();
    void closeStorage();
    bool hasAccount(const uint id) const;
    uint storeAccount(const QVariantMap & nv_pairs);
    void updateAccount(const uint id, const QVariantMap & nv_pairs);
    QVariantMap getAccount(const uint id) const;
    bool deleteAccount(const uint id);
    QList<uint> accountIds() const;
    QList<uint> findAccounts(const QVariantMap & nv_pairs) const;
    bool setValue(const uint id, const QString & key, const QVariant & value);

public Q_SLOTS:
    void walletOpened(bool success);
    void walletClosed();

private:
    KWallet::Wallet *m_wallet;
    KComponentData *m_componentData;
    bool m_walletOpen;
};

/// @endcond

#endif // header guard
