/*
 * the Decibel Realtime Communication Framework
 * Copyright (C) 2008 by George Goldberg
 *  @author George Goldberg <grundleborg@googlemail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "accountconnector.h"

#include <Decibel/accountdataserialization.h>

#include <kwallet.h>
#include <kcomponentdata.h>
#include <kdebug.h>

#include <QtCore/QMap>
#include <QtCore/QStringList>
#include <QtCore/QtPlugin>

/// @cond FALSE

/*
 * -- A little bit about how this accountconnector works.
 *
 * This connector is a basic integration into KDE. It stores
 * all the account data inside a wallet of KWallet.
 * All data is stored in a subfolder called 'decibel'
 * of the default NetworkWallet. For each
 * account handle, there is a key in this folder
 * which contains a QMap value containing all the key/value
 * pairs for the account data for that decibel account.
 * The keys inside the QMap are prepended with a
 * single character type specifier and then a ':'.
 */

AccountConnector::AccountConnector(QObject *parent) :
    AccountConnectorBase(parent),
    m_wallet(0),
    m_componentData(new KComponentData("kwalletintegration", "decibel")),
    m_walletOpen(false)
{ Q_ASSERT(0 != m_componentData); }

AccountConnector::~AccountConnector()
{
    closeStorage();
    delete m_componentData;
    delete m_wallet;
}

void AccountConnector::openStorage()
{
    kDebug() << "Using AccountConnector: kwallet.";
    // FIXME: do we have a window_id to pass to KWallet?
    m_wallet = KWallet::Wallet::openWallet(m_wallet->NetworkWallet(), 0,
                                           KWallet::Wallet::Asynchronous);
    Q_ASSERT(0 != m_wallet);
    connect(m_wallet, SIGNAL(walletOpened(bool)), this, SLOT(walletOpened(bool)));
    connect(m_wallet, SIGNAL(walletClosed()), this, SLOT(walletClosed()));
}

void AccountConnector::closeStorage()
{
    if (false == m_walletOpen) { return; }

    m_walletOpen = false;
    delete m_wallet;
    m_wallet = 0;
}

void AccountConnector::walletOpened(bool success)
{
    kDebug() << "Wallet Opened Slot Called. Success?:"
             << success;

    /*
     * if wallet has been opened successfully, we should
     * also ensure that the 'decibel' folder in it exists
     * and is set as the current one before emitting the
     * accountDataAvailable() signal.
     */
    m_walletOpen = false;
    if (success)
    {
        if(false == m_wallet->hasFolder("decibel"))
        {
            if(false == m_wallet->createFolder("decibel")) { return; }
        }
        if(false == m_wallet->setFolder("decibel")) { return; }
        m_walletOpen = true;
        emit accountDataAvailable(true);
    }
}

void AccountConnector::walletClosed()
{
    kDebug() << " Wallet was closed.";
    m_walletOpen = false;
    emit accountDataAvailable(false);
}

/*
 * does the wallet contain an account with the
 * id number this method is passed?
 */
bool AccountConnector::hasAccount(const uint id) const
{
    /*
     * first we must check that the wallet
     * is open and ready to receive stuff.
     */
    if(!m_walletOpen) { return false; }

    /*
     * the wallet is open, so we can look in
     * it to see if an account with the specified
     * id exists in it.
     */
    //kDebug() << "looking for account id:" << id;
    QString accountId;
    accountId.setNum(id);
    if(m_wallet->hasEntry(accountId))
        return true;
    else
        return false;
}

/*
 * add a new account to the wallet.
 */
uint AccountConnector::storeAccount(const QVariantMap & nv_pairs)
{
    /*
     * first we must check that the wallet is open.
     */
    if(!m_walletOpen) { return 0; }

    /*
     * now we need to find out the highest handle
     * that is free.
     */
    uint currentHandle = 0;
    QStringList accounts = m_wallet->entryList();
    if(accounts.size() == 0)
    {
        /*
         * there are no accounts, so currentHandle remains 0.
         */
        kDebug() << "there are no folders, so currentHandle=0";
    }
    else
    {
        /*
         * loop through all the handles used and
         * get the biggest. THis is not the right way
         * to chose the handle for the new account
         * but I can't think of a better way to do
         * it right now, so will have to do until
         * someone fixes it.
         */
        // FIXME: we need a better way of deciding what handle to use for the new account.
        QStringList::const_iterator end( accounts.end() );
        for ( QStringList::const_iterator itr( accounts.begin() ); itr != end; ++itr )
        {
             uint i = (*itr).toUInt();
             if(i > currentHandle)
                 currentHandle = i;
        }
    }

    /*
     * we now no the highest handle number used.
     * Increse this by 1 to get the handle number
     * for the new account we are now adding.
     */
    currentHandle++;
    QString accountHandle;
    accountHandle.setNum(currentHandle);

    /*
     * now we build the QMap containing the key/value
     * pairs for the new account.
     */
    QMap<QString, QString> accountData;
    QMap<QString, QVariant>::const_iterator end( nv_pairs.end() );
    QPair<QString, QString> kvp;
    for (QMap<QString, QVariant>::const_iterator itr(nv_pairs.begin());
            itr != end; ++itr )
    {
        // Serialize the key value.
        QByteArray valueToStore = serializeAccountDataVariant(itr.value());
        if(!valueToStore.isEmpty())
        {
            accountData.insert(itr.key(), QString(valueToStore));
        }
    }

    /*
     * we have now built the map, so we
     * insert this map into the wallet
     * using the account handle as the key
     * and the map as the value.#
     */
    if(0 == m_wallet->writeMap(accountHandle, accountData)) { return currentHandle; }
    else { return 0; }  // FIXME: is 0 the correct thing to return if an error occurs?
}

void AccountConnector::updateAccount(const uint id, const QVariantMap & data)
{
    Q_ASSERT(hasAccount(id));

    /*
     * check that the wallet is open.
     */
    if(!m_walletOpen) { return; } // FIXME: do we need to do something about the error?

    /*
     * create a QString for the accountHandle
     */
    QString accountHandle;
    accountHandle.setNum(id);

    /*
     * get the QMap containing the key/value pairs
     * for this account from the wallet.
     */
    QMap<QString, QString> accountData;
    if(0 != m_wallet->readMap(accountHandle, accountData)) { return; } // FIXME: handle the error?

    /*
     * iterate over the key/value pairs
     * that we are going to update.
     * and remove them from accountData
     * before adding the updated values.
     */
    QMap<QString, QVariant>::const_iterator end( data.end() );
    for ( QMap<QString, QVariant>::const_iterator itr( data.begin() ); itr != end; ++itr )
    {
        // Serialize the account data item value.
        QByteArray valueToStore = serializeAccountDataVariant(itr.value());

        // Check if the account data value is valid.
        if(!valueToStore.isEmpty())
        {
            /*
            * if the accountData already contains
            * a value for the current key to be
            * updated, we need to remove it.
            */
            if(accountData.contains(itr.key()))
            {
                accountData.remove(itr.key()); // FIXME: error checking
            }

            /*
            * now add the updated key/value
            * pair to the accountData.
            */
            accountData.insert(itr.key(), QString(valueToStore));
        }
    }

    /*
     * now we delete the account data from the
     * wallet for this account, and re-inesrt it
     * including the updated changes.
     */
    if(0 != m_wallet->removeEntry(accountHandle)) { return; } // FIXME: error handling?
    if(0 != m_wallet->writeMap(accountHandle, accountData)) { return; } //FIXME: error handling?
}

QVariantMap AccountConnector::getAccount(const uint id) const
{
    /*
     * first we must make sure that the wallet is open.
     */
    if(!m_walletOpen) { return QVariantMap(); }

    /*
     * set up the accountHandle QString.
    */
    QString accountHandle;
    accountHandle.setNum(id);

    /*
     * get the QMap containing the account's data
     * from the wallet, loop through it and create
     *  a QVariantMap of it.
     */
    QMap<QString, QString> accountData;
    m_wallet->readMap(accountHandle, accountData);
    QVariantMap kvp;
    QPair<QString, QVariant> kvData;
    QMap<QString, QString>::const_iterator eend( accountData.end() );
    for ( QMap<QString, QString>::const_iterator eitr( accountData.begin() ); eitr != eend; ++eitr )
    {
       kvData.first = eitr.key();
       kvData.second = deserializeAccountDataVariant(eitr.value().toAscii());

        /*
         * if we read OK, then insert the
         * key and value into the QVariantMap.
         */
        if(kvData.second != QVariant())
        {
            kvp.insert(kvData.first, kvData.second);
        }
    }
    return kvp;
}

bool AccountConnector::deleteAccount(const uint id)
{
    /*
     * first we must check that the wallet is
     * open.
     */
    if(!m_walletOpen) { return false; }

    /*
     * delete the entry from the wallet where
     * the key corresponds to the accountHandle.
     */
    QString accountHandle;
    accountHandle.setNum(id);
    if(0 == m_wallet->removeEntry(accountHandle)) { return true; }
    else { return false; }
}

QList<uint> AccountConnector::accountIds() const
{
    /*
     * first we must check that the wallet is open.
     */
    if(!m_walletOpen) { return QList<uint>(); }

    /*
     * next we get a list of all the keys that are
     * in the decibel folder of the wallet. and append
     * each one as an uint to our list.
     */
    QList<uint> accounts;
    QStringList keys = m_wallet->entryList();
    QStringList::const_iterator end( keys.end() );
    for ( QStringList::const_iterator itr( keys.begin() ); itr != end; ++itr )
    {
        bool ok;
        uint i = (*itr).toUInt(&ok);
        if(ok == false)
        {
            kDebug() << "Ignoring wallet folder.";
        }
        else
        {
            kDebug() << "Folder name:" << i;
            accounts.append(i);
        }
    }

    return accounts;
}

QList<uint> AccountConnector::findAccounts(const QVariantMap & nv_pairs) const
{
    /*
     * first we must ensure that the wallet is open.
     */
    if(!m_walletOpen) { return QList<uint>(); }

    QList<uint> results;

    /*
     * loop through all the keys (accounts) in the
     * wallet.
     */
    QStringList accounts = m_wallet->entryList();
    QStringList::const_iterator end( accounts.end() );
    for ( QStringList::const_iterator itr( accounts.begin() ); itr != end; ++itr )
    {
       /*
        * we must check the account handle is
        * valid... i.e. we can convert it from
        * a QString to a uint.
        */
        bool accountHandleOk;
        uint accountHandle = (*itr).toUInt(&accountHandleOk);
        if(accountHandleOk)
        {
            /*
            * for each account we need to see if
            * all the nv_pairs parameters
            * match.
            */
            QVariantMap accountData = getAccount(accountHandle);
            bool does_match(true);
            QPair<QString, QString> kvp;
            foreach (const QString & nv_key, nv_pairs.keys())
            {
                kvp.first = nv_key;
                kvp.second = serializeAccountDataVariant(nv_pairs[nv_key]);
                if (!accountData.contains(kvp.first) ||
                    kvp.second != accountData[kvp.first])
                {
                    does_match = false;
                    break;
                }
            }
            /*
            * if they do, then we should add that
            * account handle/id to the list of ones
            * that match (which we will return).
            */
            if (does_match)
            {
                uint result = accountHandle;
                if(result != 0)
                    results.append(result);
            }
        }
    }
    return results;
}

bool AccountConnector::setValue(const uint id,
                                const QString & key, const QVariant & value)
{
    /*
     * this is basically a convenience method
     * for updateAccount where only one
     * key/value pair is changed.
     *
     * create a QVariantMap and then
     * pass it through to updateData();
     */
    QVariantMap kv;
    kv.insert(key, value);
    updateAccount(id, kv);
    return true;
}

Q_EXPORT_PLUGIN2(accountconnector_kwallet, AccountConnector)

/// @endcond
