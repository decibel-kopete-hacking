project(decibel-plugin-accountconnector-kwallet)

find_package(KDE4 REQUIRED)
include (KDE4Defaults)
find_package(Plasma REQUIRED)
add_definitions (${QT_DEFINITIONS} ${KDE4_DEFINITIONS})

SET(QT_DONT_USE_QTGUI "YES")
#INCLUDE(${QT_USE_FILE})

INCLUDE_DIRECTORIES(${QT_QTCORE_INCLUDE_DIR}
                    ${CMAKE_CURRENT_BINARY_DIR}
                    ${CMAKE_CURRENT_SOURCE_DIR}
                   #${DECIBEL_INCLUDE_DIR}
		    ${KDE4_INCLUDE_DIR}
)

SET(accountconnector_kwallet_SRCS
    accountconnector.cpp
)

#SET(accountconnector_kwallet_MOC_HDRS
#    accountconnector.h
#)

#QT4_WRAP_CPP(accountconnector_kwallet_MOC_SRCS ${accountconnector_kwallet_MOC_HDRS})

kde4_add_library(accountconnector_kwallet SHARED
            ${accountconnector_kwallet_SRCS}
          #  ${accountconnector_kwallet_MOC_SRCS}
)
TARGET_LINK_LIBRARIES(accountconnector_kwallet
                      ${QT_LIBRARIES}
		      ${KDE4_KIO_LIBS}
		      decibel_pluginhelper
)

# Install:
INSTALL(TARGETS accountconnector_kwallet DESTINATION ${LIB_INSTALL_DIR}/Decibel/plugins)
