/*
 * the Decibel Realtime Communication Framework
 * Copyright (C) 2008 George Goldberg <grundleborg@googlemail.com>
 *  @author George Goldberg <grundleborg@googlemail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DECIBEL_DAEMON_KDE4_CONTACTCONNECTOR_H
#define DECIBEL_DAEMON_KDE4_CONTACTCONNECTOR_H

#include <Decibel/ContactConnectorBase>

#include <akonadi/collection.h>
#include <akonadi/item.h>

#include <QtCore/QList>
#include <QtCore/QMap>

class KJob;

/**
 * @brief This is an implementation of the ContactConnector interface.
 *
 * This contact connector uses akonadi to store/retrieve the contact data.
 *
 * @author George Goldberg <grundleborg@googlemail.com>
 */
class ContactConnector : public ContactConnectorBase
{
    Q_OBJECT
    Q_INTERFACES(ContactConnectorBase)

public:
    /** @brief Constructor. */
    ContactConnector(QObject *parent = 0);

    /** @brief Destructor. */
    ~ContactConnector();

    void openStorage();

    /** @brief ContactConnector::gotContact(...) */
    void gotContact(const QString & contact_url, const quint64 cookie) const;

    /** @brief ContactConnector::contact(...) */
    void contact(const QString & contact_url, const quint64 cookie) const;

    /** @brief ContactConnector::findURI(...) */
    void findURI(const QString & uri, const quint64 cookie) const;

    /** @brief ContactConnector::getURI(...) */
    void getURIs(const QString & contact_url,
                 const QString & proto,
                 const quint64 cookie) const;

    /** @brief ContactConnector::addContact(...) */
    void addContact(const QVariantMap & new_contact, const quint64 cookie);

    /** @brief ContactConnector::setPresence(...) */
    void setPresence(const QString & contact_url,
                     const QString &,
                     const QVariantMap &,
                     const quint64 cookie);

private Q_SLOTS:
    void fetchFirstLevelCollections();
    void firstLevelCollectionsFetched(KJob * job);
    void decibelAgentCreated(KJob * job);
    void fetchDecibelContacts();
    void decibelContactsFetched(KJob * job);
    void addContactComplete(KJob * job);

private:
    QString getProtocol(const QString & uri) const;
    QString getAddress(const QString & uri) const;
    /** @brief A list of known contacts (as read from our INI file). */
    QMap<quint64, KJob*> m_jobs;
    Akonadi::Collection m_collection;
    Akonadi::Item::List m_items;
};

#endif // header guard
