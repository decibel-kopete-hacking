/*
 * the Decibel Realtime Communication Framework
 * Copyright (C) 2008 George Goldberg <grundleborg@googlemail.com>
 *  @author George Goldberg <grundleborg@googlemail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "contactconnector.h"

#include "accessattribute.h"

#include <akonadi/agentinstance.h>
#include <akonadi/agentinstancecreatejob.h>
#include <akonadi/agentmanager.h>
#include <akonadi/agenttype.h>
#include <akonadi/collectioncreatejob.h>
#include <akonadi/collectionfetchjob.h>
#include <akonadi/itemcreatejob.h>
#include <akonadi/itemfetchjob.h>
#include <akonadi/itemfetchscope.h>

#include <kabc/addressee.h>

#include <kdebug.h>

#include <QtCore/QSettings>
#include <QtCore/QStringList>
#include <QtCore/QTimer>
#include <QtCore/QtPlugin>

using namespace Akonadi;

ContactConnector::ContactConnector(QObject *parent) :
        ContactConnectorBase(parent)
{ }

ContactConnector::~ContactConnector()
{ }

void ContactConnector::openStorage()
{
    kDebug() << "Using ContactConnector: akonadi.";
    /*
     * We must first check if an instance of the vcard resource exists with the
     * name decibel, in akonadi.
     */
    Akonadi::AgentInstance::List instances = Akonadi::AgentManager::self()->instances();
    foreach ( const Akonadi::AgentInstance &instance, instances )
    {
        /*
         * See if the agent instance matches the name "decibel_local_vcard"
         */
        if(instance.name() == "decibel_local_vcard")
        {
            /*
             * The decibel resource exists. Now we must fetch the collection
             * used by that resource.
             */
            fetchFirstLevelCollections();
            return;
        }
    }
    /*
     * The decibel agent doesn't exist yet. We must create it.
     */
    Akonadi::AgentType type = Akonadi::AgentManager::self()->type("akonadi_vcard_resource");
    AgentInstanceCreateJob *createJob = new AgentInstanceCreateJob(type);
    connect(createJob, SIGNAL(result(KJob*)),
            this, SLOT(decibelAgentCreated(KJob*)));
    // use this widget as parent for the config dialog
    createJob->configure(0);  // FIXME: Configure it programatically? is this possible?
    createJob->start();
}

void ContactConnector::decibelAgentCreated(KJob * job)
{
    /*
     * Check the Job did not error.
     */
    if(job->error())
    {
        kDebug() << "A AgentInstanceCreateJob error occurred:"
                 << job->errorText();
        return;
    }
    /*
     * CollectionFetchJob successfully fetched all the collections one level
     * below the root collection. Now we must look through them and see if any
     * one of them is the Decibel collection.
     */
    AgentInstanceCreateJob *createJob
        = qobject_cast<AgentInstanceCreateJob*>(job);
    /*
     * This method should only ever be called with a CollectionFetchJob so we
     * should assert if we receive anything else.
     */
    Q_ASSERT(createJob);
    kDebug() << "Decibel Agent created successfully.";
    /*
     * Make sure the name is set correctly.
     */
    createJob->instance().setName("decibel_local_vcard");
    /*
     * Now, we must fetch the collection from the Decibel agent.
     */
    fetchFirstLevelCollections();
}

void ContactConnector::fetchFirstLevelCollections()
{
    /*
     * Start a job to get all the first level collections.
     */
    CollectionFetchJob *fetchJob
        = new CollectionFetchJob(Collection::root(),
                                 CollectionFetchJob::FirstLevel);
    connect(fetchJob, SIGNAL(result(KJob*)),
            this, SLOT(firstLevelCollectionsFetched(KJob*)));
    fetchJob->start();
}

void ContactConnector::firstLevelCollectionsFetched(KJob * job)
{
    /*
     * Check the Job did not error.
     */
    if(job->error())
    {
        kDebug() << "A CollectionFetchJob error occurred:" << job->errorText();
        return;
    }
    /*
     * CollectionFetchJob successfully fetched all the collections one level
     * below the root collection. Now we must look through them and see if any
     * one of them is the Decibel collection.
     */
    CollectionFetchJob *fetchJob = qobject_cast<CollectionFetchJob*>(job);
    /*
     * This method should only ever be called with a CollectionFetchJob so we
     * should assert if we receive anything else.
     */
    Q_ASSERT(fetchJob);
    /*
     * Get the list of collections found.
     */
    Collection::List collections = fetchJob->collections();
    /*
     * Iterate through the list until we find a collection whose name matches
     * the Decibel temporary collection's name.
     */
     foreach(const Collection &collection, collections)
     {
        if(collection.name() == "decibel_local_vcard")
        {
            /* Set member variable to store decibel collection. */
            m_collection = collection;
            // FIXME: monitor collection for changes.
            /* Fetch all items in the collection. */
            fetchDecibelContacts();
            return;
        }
    }
    /*
     * If we reach here, the decibel collection doesn't exist. Ugh!
     */
     qDebug() << "Decibel collection doesn't exist, even though the temporary"
              << "agent does exist. Argh!";
}

void ContactConnector::fetchDecibelContacts()
{
    // m_collection must not be an invalid collection now.
    Q_ASSERT(m_collection != Collection());
    /*
     * Fetch all the items in the Decibel collection.
     */
    ItemFetchJob *fetchJob = new ItemFetchJob(m_collection);
    fetchJob->fetchScope().fetchFullPayload();
    fetchJob->fetchScope().fetchAllAttributes();
    connect(fetchJob, SIGNAL(result(KJob*)),
            this, SLOT(decibelContactsFetched(KJob*)));
    fetchJob->start();
}

void ContactConnector::decibelContactsFetched(KJob * job)
{
    /*
     * Check the Job did not error.
     */
    if(job->error())
    {
        kDebug() << "A ItemFetchJob error occurred:" << job->errorText();
        return;
    }
    /*
     * ItemFetchJob successfully fetched all items in the Decibel collection.
     * Cast the KJob* to a ItemFetchJob so we can call methods on it.
     */
    ItemFetchJob *fetchJob = qobject_cast<ItemFetchJob*>(job);
    /*
     * This method should only ever be called with a ItemFetchJob so we
     * should assert if we receive anything else.
     */
    Q_ASSERT(fetchJob);
    /*
     * Everything's OK. So, now we can store the items in member vars.
     */
    m_items = fetchJob->items();
    /*
     * We're now ready to go! Emit the signal to tell
     * decibel we're ready for requests and to carry on initialising.
     */
    emit contactDataAvailable(true);
}

void ContactConnector::gotContact(const QString & contact_url,
                                  const quint64 cookie) const
{
    /*
     * Loop through all the contact items, and if we find one with the same id
     * as the contact_id requested, emit the signal saying we have got that
     * contact, otherwise emit a signal saying we haven't.
     */
    quint32 contact_id(contact_url.toInt());
    foreach(const Item &item, m_items)
    {
        if(item.id() == contact_id)
        {
            emit contactGot(true, cookie);
            return;
        }
    }
    emit contactGot(false, cookie);
}

void ContactConnector::contact(const QString & contact_url,
                               const quint64 cookie) const
{
    /*
     * Loop through all the contact items until we find the one with the same id
     * as the contact_id. Then, look at the item payload and emit it's default
     * IM url. If the contact isn't found, emit a empty QVariantMap.
     */
    quint32 contact_id(contact_url.toInt());
    foreach(const Item &item, m_items)
    {
        if(item.id() == contact_id)
        {
            KABC::Addressee contact(item.payload<KABC::Addressee>());
            QVariantMap data;
            data.insert("URL", contact.custom("KADDRESSBOOK", "X-IMAddress"));
            // TODO: support other fields than URL.
            emit contactDetails(data, cookie);
            return;
        }
    }
    emit contactDetails(QVariantMap(), cookie);
}

void ContactConnector::getURIs(const QString & contact_url,
                               const QString & proto,
                               const quint64 cookie) const
{
    Q_UNUSED(proto); // FIXME: Remove this after fixing the next fixme item.
    /*
     * Loop through the contact items until we find one with the same id as the
     * contact_id. Then, get its URIs and emit them. If the contact isn't found,
     * emit an empty QStringList.
     */
    // FIXME: check the URI protocol!!!!!!
    quint32 contact_id(contact_url.toInt());
    foreach(const Item &item, m_items)
    {
        if(item.id() == contact_id)
        {
            KABC::Addressee contact(item.payload<KABC::Addressee>());
            QStringList result;
            result << contact.custom("KADDRESSBOOK", "X-IMAddress");
            // TODO: support multiple URIs.
            emit urisGot(result, cookie);
            return;
        }
    }
    emit urisGot(QStringList(), cookie);
}

void ContactConnector::findURI(const QString & uri,
                               const quint64 cookie) const
{
    /*
     * Loop through all the contact items looking for the given URI. When we
     * find it, emit the ID of the contact which it belongs to. If we don't find
     * it, emit 0.
     */
    foreach(const Item &item, m_items)
    {
        KABC::Addressee contact(item.payload<KABC::Addressee>());
        // TODO: Check other fields for IM address.
        // FIXME: Addressbook does not contain a URI, but doesn contain a field
        //        saying which type it is. Use this!
        QString app("messaging/");
        app.append(getProtocol(uri));
        if(contact.custom(app, "All") == getAddress(uri))
        {
            emit uriFound(QString::number(item.id()), cookie);
            return;
        }
    }
    emit uriFound(QString(), cookie);
}

void ContactConnector::addContact(const QVariantMap & new_contact, const quint64 cookie)
{
    kDebug() << "Add Contact Called...";
    /*
     * Create a new item, and insert it to the akonadi storage with an
     * ItemCreateJob
     */
    // Check the URI given contains a protocol.
    QString uri = new_contact.value("URI").toString();
    if(getProtocol(uri) == QString())
    {
        // We have an ERROR!
        emit contactAdded(0, cookie);
        return;
    }

    // Create an Addressee object to store the data.
    KABC::Addressee contact;

    // Store as the default IM address.
    contact.insertCustom("KADDRESSBOOK", "X-IMAddress", getAddress(uri));

    // Store as an IM address for the given protocol.
    QString app("messaging/");
    app.append(getProtocol(uri));
    contact.insertCustom(app, "All", getAddress(uri));

    // Add Nickname field with the contact's alias.
    contact.setNickName(new_contact.value("Name").toString());

    // FIXME: Add fields other than just URL and nickname

    // Set up the Item's AccessAttribute.
    // FIXME: We currently assume that the contact is on the remote server
    // already, and thats why we are adding it.
    Decibel::AccessAttribute *accessAttribute = new Decibel::AccessAttribute(true, true, false);

    // Create the new item.
    Akonadi::Item item;

    // Set its attributes.
    item.addAttribute(accessAttribute);

    // Store the new item into akonadi.
    item.setMimeType("text/directory");
    item.setPayload<KABC::Addressee>(contact);
    ItemCreateJob *createJob = new ItemCreateJob(item, m_collection);
    connect(createJob, SIGNAL(result(KJob*)),
            this, SLOT(addContactComplete(KJob*)));
    /* m_jobs must not already contain one for this cookie */
    Q_ASSERT(!m_jobs.contains(cookie));
    m_jobs.insert(cookie, createJob);
    createJob->start();
}

void ContactConnector::addContactComplete(KJob * job)
{
    /*
     * First, we must check that m_jobs contains a mapping to this job.
     */
     quint64 cookie = m_jobs.key(job);
     Q_ASSERT(cookie);
     /*
     * Check the Job did not error.
     */
    if(job->error())
    {
        kDebug() << "A ItemFetchJob error occurred:" << job->errorText();
        m_jobs.remove(cookie);
        /* Adding contact failed, so emit 0 as contact_id */
        emit contactAdded(0, cookie);
        return;
    }
    /*
     * ItemCreateJob successfully created the new contact in akonadi. We now
     * cast the KJob to an ItemCreateJob, as this method should only ever be
     * be called by one of them.
     */
    ItemCreateJob *createJob = qobject_cast<ItemCreateJob*>(job);
    /*
     * This method should only ever be called with a ItemCreateJob so we
     * should assert if we receive anything else.
     */
    Q_ASSERT(createJob);
    /*
     * Check the job did not fail.
     */
    Item item = createJob->item();
    Q_ASSERT(item != Item());
    /*
     * Emit the ID for the item.
     */
    m_jobs.remove(cookie);
    emit contactAdded(QString::number(item.id()), cookie);
}

void ContactConnector::setPresence(const QString & contact_url,
                                   const QString & presence_state,
                                   const QVariantMap & presence_params,
                                   const quint64 cookie)
{
    Q_UNUSED(cookie);
    Q_UNUSED(presence_params);
    // NOTE: DISABLE THIS METHOD, as its going to be rewritten anyway.
    // if (int(contact_id) > m_contacts.size() ||
    //     0 == contact_id) { return; }

    kDebug() << contact_url << "has a new presence:"
             << presence_state; //<< message;
}

QString ContactConnector::getProtocol(const QString & uri) const
{
    /*
     * return the protocol from a URI
     */
    int pos = uri.indexOf(QString("://"));
    if (pos < 0)
    {
        return QString();
    }
    return uri.left(pos);
}

QString ContactConnector::getAddress(const QString & uri) const
{
    /*
     * Return the address from a URI.
     */
    int pos = uri.indexOf(QString("://"));
    if (pos < 0)
    {
        return uri;
    }
    return uri.right(uri.size()-3-getProtocol(uri).size());
}

Q_EXPORT_PLUGIN2(contactconnector_akonadi, ContactConnector)
